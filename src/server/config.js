module.exports = {
  http: {
    port: process.env.PORT || 8888
  },

  auth: {
    secret: process.env.AUTH_SECRET || "secret",
    timeout: 1000,
    debug: true
  },

  connection: {
    debug: true
  },

  storage: {
    driver: require('./drivers/storage-file'),
    directory: './storage',
    maxTrxRetries: 4,
    debug: true
  },

  alpaca: {
    accountPollInterval: 5000,
    polygon: true,
    debug: true,
    paper: true
  },

  polygon: {
    key: process.env.ALPACA_KEY,
    tickerPollInterval: 5000,
    lastCloseSymbol: 'SPY',
    debug: false
  },

  ticker: {
    debug: true
  },

  graphql: {
    path: '/graphql',
    graphiql: true
  }
}
