require('module-alias/register')

const services = require('./services')
const app = require('./app')

const srverapp = app()

/**
 * Runs initialization functions and then post-init functions
 * if any are returned during init process.
 */
services.map(fn => fn(srverapp))
  .filter(fn => fn)
  .forEach(fn => fn(srverapp))
