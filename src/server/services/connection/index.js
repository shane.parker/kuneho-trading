const expressWS = require('express-ws')

const { json, uuid } = require('@util')

const stream = app => {
  const { path = '/connect', debug = false } = app.config.connection || {}
  const { http, pubsub } = app

  app.log('Initializing connection sevice on path', path)
  expressWS(http)

  const sockets = {}

  http.ws(path, async socket => {
    const id = uuid()
    if ( debug ) {
      app.log('Connection opened with ID:', id)
    }
    sockets[id] = socket

    /**
     * Establish a channel to be used for forwarding messages
     * to the client.
     */
    const send = `connection.${id}.send`
    const recv = `connection.${id}.recv`
    const ctrl = `connection.${id}.ctrl`

    const sendSubscription = pubsub.subscribe(send, ({ data }) => {
      socket.send(json(data))
    })

    /**
     * Listen for control commands.
     */
    const ctrlSubscription = pubsub.subscribe(ctrl, ({ data: { close } }) => {
      if ( close ) {
        app.log('Closing connection by request:', id)
        const sock = sockets[id]
        if ( sock ) {
          sock.close(1000, json(close.reason))
        }
      }
    })

    /**
     * Listen for messages from client and forward them
     * to the receive channel.
     */
    socket.on('message', msg => {
      try {
        pubsub.publish(recv, JSON.parse(msg))
      } catch ( err ) {
        app.log('JSON parse error from connection', id, ':', err)
        socket.close(1003, json({error: 'invalid message'}))
      }
    })

    /**
     * Announce connection
     */
    pubsub.publish('connection.opened', { id, send, recv, ctrl })

    /**
     * Remove subscription on disconnect and notify listeners
     * of connection close.
     */
    socket.on('close', () => {
      app.log('Connection closed:', id)

      pubsub.publish(['connection.closed', `connection.closed.${id}`], { id })

      ctrlSubscription.unsubscribe()
      sendSubscription.unsubscribe()

      delete sockets[id]
    })
  })
}

module.exports = stream
