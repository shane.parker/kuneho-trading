
const storage = require('./storage')
const pubsub = require('./pubsub')
const http = require('./http')
const graphql = require('./graphql')
const connection = require('./connection')
const alpaca = require('./alpaca')
const polygon = require('./polygon')
const ticker = require('./ticker')
const auth = require('./auth')

const services = [
  pubsub,
  storage,
  http,
  connection,
  auth,
  graphql,
  polygon,
  ticker,
  alpaca
]

module.exports = services
