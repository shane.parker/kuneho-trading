
const account = require('./account')
const realtime = require('./realtime')
const trades = require('./trades')

const alpacaService = app => {
  app.log('Initializing Alpaca account service')

  /**
   *  Start up sub-services
   */
  const subservices = [account, realtime, trades]
  subservices.forEach(init => init(app))
}

module.exports = alpacaService
