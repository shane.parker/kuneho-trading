
const realtime = app => {
  const { storage } = app

  /*storage.query(v => (v.type === 'symbol') && (v.realtime === true || v.position))
    .then(results => results.map( ({value}) => `Q.${value.symbol}` ))
    .then(rtconn.subscribe)*/

  const realtimeConnection = alpaca => {
    //const marketSocket = alpaca.data_ws
    /*marketSocket.onConnect( () => {
      app.log('Alpaca market websocket connected')
    })
    marketSocket.onDisconnect(() => {
      app.log("Alpaca market websocket disconnected.")
    })
    marketSocket.onStockTrades((subject, data) => {
      app.log(`Stock trades: ${subject}, price: ${data.price}`)
    })
    marketSocket.onStockQuotes(async (_subject, data) => {
      const parsed = JSON.parse(data)
      return
      
      const pending = parsed.map(q => storage.transaction(async trx => {
        const current = await trx.get(q.sym)
        const updated = {
          ...current,
          quote: {
            timestamp: Date.now(),
            ask: { price: q.ap, size: q.as },
            bid: { price: q.bp, size: q.bs }
          }
        }
        trx.set(q.sym, updated)
      }))

      await Promise.all(pending.values())
    })
    marketSocket.onStockAggSec((subject, data) => {
      app.log(`Stock agg sec: ${subject}, ${data}`)
    })
    marketSocket.onStockAggMin((subject, data) => {
      app.log(`Stock agg min: ${subject}, ${data}`)
    })
    marketSocket.connect()*/

    return immutable({
      //subscribe: channels => marketSocket.subscribe(channels)
    })
  }
}

module.exports = realtime
