/**
 * Get total of positions across all data sources
 * for a symbol.
 */
const totalPositions = positions =>
  Object.values(positions).reduce((acc, v) => acc + v.quantity,0)

/**
 * Tidies up the symbols we don't want to see on the dashboard.
 */
tidy = app => app.pubsub.subscribe('storage.updated', ({ data: { id, value } }) => {
  if ( value.position && !value.watching && (totalPositions(value.positions) <= 0) ) {
    app.log('Removing symbol:', id)
    storage.delete(id)
  }
})

module.exports = tidy
