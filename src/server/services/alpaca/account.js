const Alpaca = require('@alpacahq/alpaca-trade-api')

const task = require('@util/task')

const account = app => {
  const {
    accountPollInterval = 5000,
    usePolygon = true,
    debug = false
  } = app.config.alpaca || {}

  const { storage, pubsub } = app
  const accountTasks = {}

  const accounts = {}

  pubsub.subscribe('account.connected', async ({ data: { channel, account } }) => {
    const { credentials: { alpaca: creds } } = account
    if ( creds === undefined ) {
      return
    }

    if ( debug ) {
      app.log('Subscribing to alpaca account events for:', account.id)
    }

    const alpaca = new Alpaca({
      keyId: creds.key,
      secretKey: creds.secret,
      paper: creds.paper,
      usePolygon
    })
    accounts[account.id] = alpaca

    const symbolsub = pubsub.subscribe(`query.symbols.list.${account.id}`, () =>
      alpaca.getPositions()
        .then( r => ({
          source: 'alpaca',
          symbols: r.map(pos => ({
            symbol: pos.symbol,
            position: parseInt(pos.qty)
          }))
        })))

    const discsub = pubsub.subscribe(`account.disconnected.${account.id}`, () => {
      if ( debug ) {
        app.log('Unsubscribing from alpaca account data for', account.id)
      }

      symbolsub.unsubscribe()
      discsub.unsubscribe()
    })
  })

    /*const hash = `${alpaca.key}_${alpaca.secret}_${alpaca.paper}`
    if ( hash in accountTasks ) {
      return
    }

    pubsub.publish('account.connected.alpaca', { account, channel, alpaca: connection })

    const accountTask = self => {
      self.once(() => {
        const subscription = pubsub
          .subscribe(`user.disconnected.${acctId}`, () => self.stop())

        self.shutdown(() => {
          pubsub.publish('account.disconnected.alpaca', { account, channel, alpaca: connection })
          subscription.unsubscribe()
          delete accountTasks[hash]
        })
      })

      /**
       * Send a snapshot of account details.
       */
      /*const snapshot = connection.getAccount().then( acct => {
        if ( debug ) {
          self.log('Fetched account data:', acct)
        }
  
        pubsub.publish(channel, {
          channel: 'account.snapshot',
          data: {
            id: acct.id,
            type: 'account',
            source: 'alpaca',
            cash: acct.cash,
            equity: acct.equity,
            currency: acct.currency,
            daytrades: acct.daytrade_count,
            daytrade_buying_power: acct.daytrading_buying_power,
            market_value: acct.long_market_value
          }
        })
      })
    }

    accountTasks[hash] = task(accountTask, {
      name: `Alpaca Account Task (${alpaca.key})`,
      interval: accountPollInterval,
      started: true
    })
  }*/
}

module.exports = account
