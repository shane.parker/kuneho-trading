const trades = (app, alpaca) => {
  //const socket = alpaca.trade_ws
  return
  
  socket.onConnect(() => {
    app.log("Alpaca account websocket connected")
    socket.subscribe(['trade_updates', 'account_updates'])
  })
  socket.onDisconnect(() => {
    app.log("Alpaca account websocket disconnected.")
  })
  socket.onStateChange(newState => {
    app.log(`Alpaca account websocket state changed to ${newState}`)
  })
  socket.onOrderUpdate(data => {
    app.log(`Order updates:`, data)
    
    importPositions()
      .then(subscribeRealtime)
  })
  socket.onAccountUpdate(data => {
    app.log(`Account updates:`, data)
  })
  socket.connect()
}

module.exports = trades
