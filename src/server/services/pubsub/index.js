
const { immutable } = require('@util')

const pubsub = app => {
  const channelMap = {}

  const chlist = ch => Array.isArray(ch) ? ch : [ch]

  const self = immutable({
    subscribe: (channels, listener) => {
      chlist(channels).map( channel => 
        channelMap[channel] || (channelMap[channel] = new Set()) )
          .forEach(channel => channel.add(listener))

      return immutable({
        subscribe: () => self.subscribe(channels, listener),
        unsubscribe: () => self.unsubscribe(channels, listener)
      })
    },

    unsubscribe: (channels, listener) =>
      chlist(channels).map(ch => channelMap[ch])
        .filter(v => v !== undefined).forEach( channel => 
          channel.delete(listener)),

    query: async (channels, data) => {
      const plist = chlist(channels).map(chName => [chName, channelMap[chName]])
        .filter( ([_, ch]) => (ch !== undefined) && (ch.size > 0))
          .map( ([name, channel]) => {
            const msg = { channel: name, data }
            return Array.from(channel).map(async l => l(msg))
          }).flat()

      return Promise.allSettled(plist)
          .then(settled => settled.filter(r => r.value || (r.reason && app.log('Error during pubsub query:', r.reason))))
          .then(r => r.flatMap(r => r.value))
    },

    publish: (channels, data) =>
      chlist(channels).map(chName => [chName, channelMap[chName]])
        .filter(([_, ch]) => (ch !== undefined) && (ch.size > 0))
        .forEach(([name, channel]) => {
          const msg = { channel: name, data }
          channel.forEach(async l => l(msg))
        })
  })

  app.pubsub = self
}

module.exports = pubsub
