const fetch = require('node-fetch')
const queue = require('@util/queue')

/**
 * Polls market status and responds to queries about
 * current status info.
 */
const details = app => {
  const {
    key,
    marketsInterval = 30000,
    lastCloseSymbol = 'AAPL',
    debug = false
  } = app.config.polygon || {}

  const { pubsub } = app
  const req = queue('Polygon Market Status Service')
  let current

  /**
   * NOTE: We fetch the last day the market was open here by quoting close date
   * on a well known stock. Probably there is a better way to do this, but it
   * accounts for holidays, etc.
   */
  const fetchStatus = () => {
    app.log('Fetching market status.')

    const urls = [
      `https://api.polygon.io/v1/marketstatus/now?apiKey=${key}`,
      `https://api.polygon.io/v2/aggs/ticker/${lastCloseSymbol}/prev?apiKey=${key}`
    ]

    if ( debug ) {
      app.log('fetching', urls)
    }

    return Promise.allSettled(urls.map(url => fetch(url)))
      .then(r => Promise.allSettled(r.map( ({ value, reason }) =>
        (value && value.json()) || app.log(reason))))
      .then(r => r.map( ({ value, reason }) => value || app.log(reason)))
      .then( ([ status, lastClose ]) => {
        if ( !current || (current.market !== status.market) ) {
          const lclose = (lastClose && lastClose.results[0]) || {}

          current = {
            type: 'market_status',
            status: status.market,
            timestamp: Date.now(),
            exchanges: status.exchanges,
            currencies: status.currencies,
            lastClose: lclose ? new Date(lclose.t) :
              ((current && current.lastClose) || null)
          }

          app.log('Market status update:', current)
          pubsub.publish('markets.status.updated', current)
          req.start()

          return current
        }
      })
  }

  pubsub.subscribe('query.markets.status', () =>
    req.push(() => current ? current : fetchStatus()))

  const statusTask = () => fetchStatus().catch(err => app.log(err))
      .finally(() => setTimeout(statusTask, marketsInterval))

  statusTask()
}

module.exports = details
