const fetch = require('node-fetch')

/**
 * Polls details for symbols if needed
 */
const details = app => {
  const { key: apiKey, debug } = app.config.polygon || {}
  const { pubsub, storage } = app

  pubsub.subscribe('data.updated.symbol', async ({data: { id, key, value } }) => {
    const { details = {} } = value
    if ( details.polygon ) {
      return
    }

    const url = `https://api.polygon.io/v1/meta/symbols/${id}?apiKey=${apiKey}`
    if ( debug ) {
      app.log('Fetching', url)
    }

    const result = await fetch(url)
      .then(resp => resp.json())

    if ( debug ) {
      app.log('Got details:', result)
    }

    storage.patch(key, {
      'details.polygon': result.symbol
    })
  })
}

module.exports = details
