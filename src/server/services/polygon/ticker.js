const fetch = require('node-fetch')
const task = require('@util/task')

const moment = require('moment')


/**
 * Polls for updates for non-realtime data
 * 
 * TODO: Figure a better way to have the polling done
 * not all at once.
 */
const ticker = async app => {
  const {
    tickerPollInterval = 5000,
    key, debug = false
  } = app.config.polygon || {}

  const { storage, pubsub } = app
  const tickerTasks = {}
  let marketStatus

  /**
   * A task that polls for ticker information for a single
   * symbol. 
   */
  const tickerTask = symbol => async self => {
    self.log('Polling ticker for symbol', symbol)

    self.once(() => {
      const subscription = pubsub.subscribe(`data.deleted.symbol.${symbol}`, () => {
        if ( debug ) {
          self.log('Symbol deleted. Terminating task.')
        }
        self.stop()
      })

      self.shutdown(() => {
        subscription.unsubscribe()
      })
    })

    const fetchClosed = async () => {
      const stdate = moment(marketStatus.lastClose)
        .subtract(5, 'day').format('YYYY-MM-DD')
      const enddate = moment(marketStatus.lastClose)
        .add(1, 'day').format('YYYY-MM-DD')  

      const url = `https://api.polygon.io/v2/aggs/ticker/${symbol}/range/1/day/${stdate}/${enddate}?sory=asc&apiKey=${key}`   
      if ( debug ) {
        self.log('Fetching aftermarket data:', url)
      }

      const fetched = await fetch(url).then(r => r.json())
      if ( (fetched.status !== 'OK') || (fetched.results.length < 2) ) {
        throw Error('Invalid ticker results')
      }

      const { results } = fetched
      
      const current = results[fetched.results.length-1]
      const prev = results[fetched.results.length-2]

      return {
        current: {
          price: current.c,
          open: current.o,
          low: current.l,
          high: current.h
        },
        prev: {
          open: prev.o,
          close: prev.c,
          low: prev.l,
          high: prev.h
        }
      }
    }

    const fetchOpen = async () => {
      const url = `https://api.polygon.io/v2/snapshot/locale/us/markets/stocks/tickers/${symbol}?apiKey=${key}`
      if ( debug ) {
        self.log('Fetching:', url)
      }

      const fetched = await fetch(url).then(r => r.json())
      if ( debug ) {
        self.log('Fetched:', fetched)
      }

      if ( fetched.status !== 'OK' ) {
        throw Error('Invalid ticker results')
      }

      const { ticker: { day, prevDay, lastQuote } } = fetched
      return {
        current: {
          price: lastQuote.p,
          open: day.o,
          low: day.l,
          high: day.h
        },
        prev: {
          open: prevDay.o,
          close: prevDay.c,
          low: prevDay.l,
          high: prevDay.h
        }
      }
    }

    const tickerData = await (marketStatus.status === 'closed' ? fetchClosed() : fetchOpen())
    const updated = {
      ticker: tickerData,
      timestamp: Date.now(),
      symbol
    }

    if ( debug ) {
      self.log(`Patching symbol ${symbol} with data:`, updated)
    }

    /**
     * Publish update for symbol
     */
    pubsub.publish(`symbol.${symbol}`, updated)

    /*await storage.patch(`symbol.${symbol}`, {
      ticker: updated
    })*/
  }

  const taskForSymbol = name => 
    tickerTasks[name] || (tickerTasks[name] = task(tickerTask(name), {
      name: `Ticker Task (${name})`,
      interval: tickerPollInterval,
      started: true,
      debug
    }))

  /**
   * Get current market status and start fetching symbol ticker data
   */
  await pubsub.query(['query.markets.status'])
    .then( ([ status ]) => marketStatus = status)

  /**
   * Listen for market updates. Ticker polling strategy changes depending
   * on whether the market is open/closed.
  */
  pubsub.subscribe(['markets.status.updated'], ({ data }) => {
    console.log('Got market update:', data)
    marketStatus = data
  })

  /**
   * Fetch symbols we care about from storage and start a
   * task to poll their ticker data.
   */
  const symbols = await storage.query('symbol')
    .then(results => results.map( ({ id }) => id ))

  if ( debug ) {
    app.log('Starting ticker tasks for symbols:', symbols)
  }

  symbols.forEach(taskForSymbol)
}


module.exports = ticker
