const ticker = require('./ticker')
const details = require('./details')
const markets = require('./markets')

const polygonService = app => {
  app.log('Initializing Polygon market data service')

  /**
   *  Start up sub-services
   */
  const subservices = [markets, ticker, details]
  subservices.forEach(init => init(app))
}

module.exports = polygonService
