const express = require('express')

const http = app => {
  const { port = 8080 } = app.config.http || {}
  console.log('Starting http on', port)

  const srv = express()
  srv.get('/health', (_req, res) => res.send('ok'))
  srv.use(express.static('public'))

  app.http = srv

  return () => srv.listen(port, () => {
    console.log(`HTTP service running on port ${port}!`)
  })
}

module.exports = http
