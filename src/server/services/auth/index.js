const crypto = require('crypto');

const { except } = require('@util')
const sublist = require('@util/sublist')

const timer = require('@util/timer')
const monitor = require('@util/monitor')

const auth = app => {
  const {
    debug = false,
    timeout = 10000,
    secret
  } = app.config.auth || {}

  const { pubsub, storage } = app
  const monitors = {}

  const authenticated = async ({ account, connection, send, recv, ctrl }) => {
    if ( debug ) {
      app.log('Account login:', account.id)
    }

    const channels = sublist(pubsub)

    /**
     * Create a monitor that tracks number of connections from the account.
     * When all connections are dropped, the account is officially disconnected.
     */
    const connMon = monitors[account.id] || (monitors[account.id] = monitor(0, () => {
      pubsub.publish(['account.disconnected', `account.disconnected.${account.id}`], account)
      delete monitors[account.id]

      if ( debug ) {
        app.log('Account disconnected:', account.id)
      }
    }))

    /**
     * Send account details to connection. We filter out credentials
     * here for security.
     * 
     * TODO: Consider storing credentials in a separate secured service.
     */
    pubsub.publish(send, {
      status: 'ok',
      account: {...account, credentials: undefined}
    })

    /**
     * Create account-wides channel for messages.
     */
    const channel = `account.${account.id}`
    channels.subscribe(channel, async ({ data }) =>
      pubsub.publish(send, data))

    /**
     * Watch for disconnect and notify services.
     */
    channels.subscribe(`connection.closed.${connection}`, () => {
      channels.unsubscribe()
      connMon.decrement()
    })

    /**
     * Announce presence of account login and allow services to initialize
     * their internal state if this is the first account connection.
     */
    if ( connMon.count === 0 ) {
      try {
        await pubsub.query('account.connected', { account, channel })
      } catch ( err ) {
        app.log('Error during account.connected query:', err)
      }

      pubsub.publish(['account.connected.ready', `account.connected.ready.${account.id}`], { account, channel })
    }

    /**
     * Keep track of number of account connections.
     */
    connMon.increment()
  }

  pubsub.subscribe('connection.opened', ({ data: { id: connection, send, recv, ctrl } }) => {
    /**
     * Disconnect after some time without a login request.
     */
    const disconnectTimer = timer(timeout)
    disconnectTimer.wait().then(() =>
      pubsub.publish(ctrl, {
        close: {
          reason: 'timeout'
        }
      }))

    /**
     * Listen for the authentication request.
     */
    const loginSubscription = pubsub.subscribe(recv, async ({ data: { login } }) => {
      try {
        const { email, password } = login || except('Invalid login')
        if ( !email || !password ) {
          throw Error('Invalid login request')
        }

        const hashed = crypto.createHmac('sha256', secret)
          .update(password).digest('hex')
          
        const lowerEmail = email.toLowerCase()

        const [{value: account}] = await storage.query('account', ({ email, credentials: { kuneho } }) => {
          if ( kuneho.hash !== hashed ) {
            return false
          }

          return (email.toLowerCase() === lowerEmail)
        })
        
        if ( account === undefined ) {
          throw Error('Invalid username or password.')
        }

        disconnectTimer.cancel()
        loginSubscription.unsubscribe()
        closedSub.unsubscribe()

        authenticated({ connection, account, send, recv })
      } catch ( err ) {
        if ( debug ) {
          app.log('Authentication error:', err.message)
        }

        pubsub.publish(send, { status: 'error', reason: err.message })
      }
    })

    const closedSub = pubsub.subscribe(`connection.closed.${connection}`, () => {
      if ( debug ) {
        app.log('Connection closed before login:', connection)
      }
      
      loginSubscription.unsubscribe()
      closedSub.unsubscribe()
    })
  })
}

module.exports = auth
