const { sleep } = require('@util')

const util = require("util")

const ticker = app => {
  const {
    debug = false
  } = app.config.ticker || {}

  const { pubsub } = app

  app.log('Initializing ticker service')

  pubsub.subscribe('account.connected.ready', async ({ data: { channel, account } }) => {
    /**
     * Query the list of symbols and merge them together.
     */
    const symbols = await pubsub.query(`query.symbols.list.${account.id}`)
      .then( results => results.reduce( (acc, { source, symbols }) => {
        symbols.forEach( ({ symbol, position }) => {
          const entry = acc[symbol] || (acc[symbol] = {
            /**
             * Forward symbol updates to the account.
             */
            subscription: pubsub.subscribe(`symbol.${symbol}`, ({ data }) =>
              pubsub.publish(channel, data)),

            /**
             * Keep a map of positions from source => quantity
             */
            positions: {}
          })

          if ( position > 0 ) {
            const current = entry.positions[source] || { quantity: 0 }
            entry.positions[source] = {
              ...current,
              quantity: current.quantity + position
            }
          }
        })

        return acc
      }, {}))

    /**
     * Clean up the subscriptions when the account is disconnected.
     */
    pubsub.subscribe(`account.disconnected.${account.id}`, () => 
      Object.values(symbols).forEach( ({ subscription }) => subscription.unsubscribe()))

    if ( debug ) {
      app.log('Watching symbols for account:', account, util.inspect(symbols, false, Infinity))
    }
  })
}

module.exports = ticker