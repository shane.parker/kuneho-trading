/**
 * Field to store the version hash used
 * for optimistic locking.
 */
const Version = '$version'

/**
 * Thrown when a consistency error is detected on commit
 */
const CommitError = Symbol('Commit Sync Error')

/** 
 * Splits an object key into an type/id pair
*/
const splitTarget = target => {
  const idx = target.indexOf('.')
  return (idx >= 0) ?
    [target.substring(0, idx), target.substring(idx+1)] :
      ['object', target]
}

module.exports = {
  Version,
  CommitError,
  splitTarget
}
