const { uuid, clone, immutable, mutate, valueOf, except } = require('@util')
const cache = require('@util/cache')

const { Version, CommitError, splitTarget } = require('./constants')

/**
 * Default null storage driver that just implements
 * the required interface.
 */
const nullDriver = () => ({
  set: (_id, v) => (v, true),
  get: id => null,
  has: key => false,
  commit: objs => true,
  delete: id => true,
  patch: (id, cb) => false,

  iterator: () => ({
    next: () => null,
    close: () => true
  }),

  get name() {
    return 'null'
  }
})

const storage = app => {
  const config = app.config.storage || {}
  const { maxTrxRetries = 4, debug = false } = config
  const { pubsub } = app

  const objcache = cache({ debug })

  app.log('Initializing storage service')

  const driver = (config.driver || nullDriver)(app)
  app.log('Using storage driver:', driver.name)

  /**
   * Listen fro precommit messages and broadcast updates.
   */
  pubsub.subscribe('storage.driver.precommit', ({ data: { writeList, deleteList } }) => {
    writeList.forEach( ([key, value]) => {
      const [type, id] = splitTarget(key)
      pubsub.publish(['data.updated', `data.updated.${type}`, `data.updated.${key}`], {
        key, value, type, id
      })
    })
    deleteList.forEach( ([key, value]) => {
      const [type, id] = splitTarget(key)
      pubsub.publish(['data.deleted', `data.deleted.${type}`, `data.deleted.${key}`], {
        key, value, type, id
      })
    })
  })

  const runQuery = async (type, query, cb) => {
    if ( typeof type === 'function' ) {
      query = type
      type = 'object'
    }

    if ( query === undefined) {
      query = () => true
    }

    const iter = await driver.iterator(type)
    for await ( const next of iter ) {
      const { id, key, value } = next
  
      try {
        const loaded = await value()
        if ( query(loaded, id) ) {
          await cb({id, value: loaded, key, type})
        }
      } catch ( err ) {
        app.log(err)
      }
    }
  }

  const self = immutable({
    get: async (key, defaultValue) => {
      const cached = objcache.get(key)
      if ( cached ) {
        if ( debug ) {
          console.log('cached', key)
        }
        return cached
      }

      try {
        const value = await driver.get(key)
        objcache.set(key, value)
        return value
      } catch ( err ) {
        if ( defaultValue !== undefined ) {
          return defaultValue
        }

        throw err
      }
    },

    set: (key, value) => {
      const versioned = {...clone(value), [Version]: uuid()}
      objcache.set(key, value)

      /**
       * NOTE: Taking an optimistic approach here and assuming
       * the data hits permanent storage...
       */
      const [type, id] = splitTarget(key)
      app.pubsub.publish(['data.updated', `data.updated.${type}`, `data.updated.${key}`], {
        key, value: versioned, type, id
      })

      driver.set(key, versioned)
    },

    delete: async (id, type) => {
      const value = await self.get(id, null)
      if ( value !== null ) {
        driver.delete(id, type)
          .then( () => {
            const [type, id] = splitTarget(key)
            app.pubsub.publish(['data.deleted', `data.deleted.${type}`, `data.deleted.${key}`], {
              id, value, type, key
            })
          })
          .then( () => app.log('deleted key', id))
      }
    },

    query: async (type, query) => {
      const results = []

      try {
        await runQuery(type, query, v => results.push(v))
      } catch (err) {
        console.log(err)
        return []
      }

      return results
    },

    transaction: async (fn, retries = maxTrxRetries) => {
      const t1 = Date.now()
      while ( (retries--) > 0 ) {
        try {
          const writeSet = new Map()
          const readSet = new Map()
          const deleteSet = new Map()

          const trx = {
            get: async (key, defaultValue) => {
              const obj = await self.get(key, defaultValue)
              readSet.set(key, obj[Version])
              return obj
            },
            set: (key, val) => writeSet.set(key, clone(val)),
            delete: async key => {
              const val = await self.get(key, null)
              if ( val !== null ) {
                deleteSet.set(key, val)
              }
            },
            patch: async (id, fn, upsert)  => {
              const current = await trx.get(id, upsert ? {} : undefined)
      
              const updated = fn(current)
              if ( updated === undefined ) {
                throw Error('patch callback must return value')
              }

              trx.set(id, updated)
            },
            query: async (type, fn) => {
              const results = []
              await runQuery(type, fn, ({id, value}) => {
                readSet.set(id, obj[Version])
                results.push({id, value})
              })
              return results
            }
          }

          const result = await fn(trx)
          await driver.commit(readSet, writeSet, deleteSet)

          objcache.setEntries(writeSet.entries())
          objcache.deleteKeys(deleteSet.keys())

          return result
        } catch ( err ) {
          if ( err === CommitError ) {
            if ( debug ) {
              app.log('Commit sync error:', err)
            }
            continue
          }

          if ( debug ) {
            app.log('Transaction error:', err)
          }

          throw err
        } finally {
          if ( debug ) {
            const nt = Date.now() - t1
            app.log('Transaction completed in', nt, 'milliseconds with', maxTrxRetries - retries, 'tries')
          }
        }
      }

      throw Error(`Max transaction retries reached: ${maxTrxRetries}`)
    },

    patch: (id, value, upsert) => {
      const mapped = valueOf(() => {
        if (typeof id === 'string')  {
          return {[id]: value}
        }
        upsert = value
        return id
      })
      
      return Promise.all(Object.entries(mapped).map( ([id, data]) => {
        const fn = (typeof data === 'object') ? 
          current => mutate(current, data) : (typeof data === 'function') ? 
            data : except(`Invalid patch value: ${data}`)
        
        self.transaction(async trx => {
          const current = await trx.get(id, upsert ? {} : undefined)
          const updated = fn(current, id)

          if ( updated === undefined ) {
            throw Error('patch callback must return value')
          }

          trx.set(id, updated)
        })
      }))
    },

    update: async (type, filter, val) => {
      if ( val === undefined ) {
        throw Error('No update value/function defined')
      }

      if ( typeof type === 'function' ) {
        filter = type
        type = 'object'
      }

      let count = 0
      await runQuery(type, filter, async ({id, value, key}) => {
        if ( (typeof val) === 'function' ) {
          try {
            const nval = val({id, value, key})
            await self.patch(key, nval)
          } catch ( err ) {
            console.log(err)
          }
        } else {
          await self.set(id, val)
        }
        count++
      })

      return count
    },

    insert: async entries => {
      entries = Array.isArray(entries) ? entries : [entries]

      let count = 0
      for ( const { id = uuid(), value } of entries ) {
        if ( await driver.has(id) ) {
          throw Error(`Unable to insert object: ${id}. Duplicate id`)
        }
        
        await self.set(id, value)
        count++
      }

      return count
    }
  })

  app.storage = self
}

module.exports = storage
