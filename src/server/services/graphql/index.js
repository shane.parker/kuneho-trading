const { buildSchema } = require('graphql')
const graphqlHTTP = require('express-graphql')

var schema = buildSchema(`
  type Query {
    hello: String
  }
`);

const gql = app => {
  const {
    path = '/graphql',
    graphiql = false
  } = app.config.graphql || {}

  app.log('Initializing GraphQL service')

  const root = {
    hello: () => 'yay!'
  }

  app.http.use(path, graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql,
  }))
}

module.exports = gql
