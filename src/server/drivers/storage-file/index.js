const { Version, CommitError, splitTarget } = require('@services/storage/constants')
const { immutable, uuid } = require('@util')

const queue = require('@util/queue')

const fs = require('fs')
const path = require('path')
const os = require('os')

const fileStorage = app => {
  const { directory = './storage', debug } = app.config.storage || {} 
  const { pubsub } = app

  const targetDirName = type => `${directory}${path.sep}${type}${path.sep}`
  const targetName = (type, id) => `${targetDirName(type)}${id}`

  /**
   * Queue of pending file IO requests
   */
  const ioq = queue('Storage Service IO Queue', true)

  const output = (target, data) => new Promise( (resolve, reject) => {
    const tmpname = path.join(os.tmpdir(), `${uuid()}.tmp`)
    fs.writeFile(tmpname, data, err => {
      if ( err ) {
        app.log(err)
        reject(err)
      }

      const [type, id] = splitTarget(target)
      const dirpath = targetDirName(type)
      
      const rename = () => new Promise((resolve, reject) => {
        fs.rename(tmpname, targetName(type, id), err => {
          if ( err ) {
            app.log(err)
            reject(err)
          }

          resolve(true)
        })
      })

      fs.exists(`${directory}/${type}`, exists => {
        if ( exists ) {
          rename().then(resolve).catch(reject)
        } else {
          if ( debug ) {
            app.log('Creating type directory:', dirpath)
          }

          fs.mkdir(dirpath, err => {
            if ( err ) {
              return reject(err)
            }
            rename().then(resolve).catch(reject)
          })
        }
      })
    })
  })

  const input = target => new Promise( (resolve, reject) => {
    const [type, id] = splitTarget(target)
    
    fs.readFile(targetName(type, id), (err, data) => {
      if ( err ) {
        if ( debug ) {
          console.log(target, err)
        }
        return reject(err)
      }
      
      try {
        resolve(JSON.parse(data))
      } catch ( err ) {
        reject(err)
      }
    })
  })

  const tryInput = target => new Promise( resolve => 
    input(target).then(resolve).reject(() => resolve(null)))

  const remove = target => new Promise( (resolve, reject) => {
    const [type, id] = splitTarget(target)

    fs.unlink(targetName(type, id), err => {
      if ( err ) {
        if ( debug ) {
          app.log('Cannot delete', target, err)
        }
        return reject(err)
      }

      if ( debug ) {
        app.log('Deleted', id)
      }
      resolve(true)
    })      
  })

  /**
   * Performs an optimistic locking commit on a set of objects.
   * If any object has a version mismatch, the commit
   * fails and no objects are updated.
   */
  const doCommit = async (readSet, writeSet, deleteSet) => {
    const workingSet = new Map()

    /**
     * Make sure none of the read objects have changed
     * versions since commit began.
     */
    for ( const [key, version] of readSet ) {
      const current = (version !== undefined) ? await input(key) : {}
      if ( current[Version] !== version) {
        if ( debug ) {
          app.log('Commit version mismatch (read):', version, current[Version])
        }

        throw CommitError
      }

      workingSet.set(key, current)
    }

    /**
     * Iterate over objects to write and fetch the version
     * in storage to compare again.
     */
    for ( const [key, value] of writeSet ) {
      /**
       * Get current version of changed object from driver
       */
      const current = workingSet.get(key) || (await tryInput(key))
      const version = current ? current[Version] : undefined

      /**
       * If the version is mismatched, the commit must fail.
       */
      if ( version !== value[Version] ) {
        if ( debug ) {
          app.log('Commit version mismatch (write):', version, value[Version])
        }

        throw CommitError
      }
    }

    const writeList = Array.from(writeSet.entries())
    const deleteList = Array.from(deleteSet.entries())

    /**
     * Broadcast a pre-commit message and then write changes to disk.
     */
    pubsub.publish('storage.driver.precommit', {writeList, deleteList})
    await Promise.all([
      ...writeList.map(([key, value]) => output(key, JSON.stringify(value))),
      ...deleteList.map(([key]) => remove(key))
    ])
  }

  /**
   * Make sure the storage directory exists
   */
  if ( fs.existsSync(directory) === false ) {
    app.log('Creating storge directory:', directory)
    fs.mkdirSync(directory);
  }

  const self = immutable({
    set: (key, value) => new Promise( (resolve, reject) => {
      ioq.push(() => output(key, JSON.stringify(value))
        .then(resolve)
        .catch(reject))
    }),

    get: key => new Promise( (resolve, reject) => {
      ioq.push(() => input(key)
        .then(resolve)
        .catch(reject))
    }),

    has: key => new Promise( resolve => {
      ioq.push(() => fs.exists(key, v => resolve(v)))
    }),

    delete: key => new Promise( (resolve, reject) => {
      const target = targetName(key)
      ioq.push(() => remove(key)
        .then(resolve)
        .catch(reject))
    }),

    commit: (readSet, writeSet, deleteSet) => new Promise( (resolve, reject) => {
      ioq.push(() => doCommit(readSet, writeSet, deleteSet)
        .then(resolve)
        .catch(reject))
    }),

    iterator: type => new Promise( (resolve, reject) => {
      fs.opendir(targetDirName(type), (err, dir) => {
        if ( err ) {
          app.log(err)
          reject(err)
        }
        
        resolve({
          [Symbol.asyncIterator]: () => ({
            next: async () => {
              for ( ;; ) {
                const nentry = await dir.read()
                if ( nentry === null ) {
                  dir.close()
                  return { done: true }
                }

                const id = nentry.name
                if ( (id[0] !== '.') && nentry.isFile() ) {
                  const key = `${type}.${id}`
                  return {
                    value: {
                      value: () => self.get(key),
                      key: `${type}.${id}`,
                      id
                    },
                    done: false
                  }
                }
              }
            }
          })
        })
      })
    }),
  
    get name() {
      return 'file'
    }
  })

  return self
}

module.exports = fileStorage
