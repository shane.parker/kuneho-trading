const config = require('@config')

const app = () => {
  const self = {
    get config() {
      return config
    },

    log: (...params) => {
      console.log(...params)
    }
  }

  return self
}

module.exports = app
