import connection from '../connection'
import ui from '../ui'

import symbolComponent from '../components/symbol'
import headerComponent from '../components/header'
import footerComponent from '../components/footer'

const dashboard = () => {
  const symbols = {}
  const accounts = {}

  let positions = {}
  let marketStatus = {}
  let account

  /**
   * Removes a symbol from the UI
   */
  const detachSymbol = id => {
    delete symbols[id]
    ui.detach(`.symbols/${id}`)
  }
  
  /**
   * Update header with account info
   */
  const updateHeader = () => {
    ui.component(`.header/header`, { accounts, marketStatus }, headerComponent)
  }

  /**
   * Updates the UI for a given symbol
   */
  const updateSymbols = data => {
    const update = data || Object.values(symbols)
    if ( update ) {
      update.forEach(s => symbols[s.symbols] = s)
    }

    Object.values(update).forEach( data => 
      ui.component(`.symbols/${data.symbol}`, { positions, data, marketStatus }, symbolComponent))
  }

  /**
   * Updates the footer
   */
  const updateFooter = () => {
    ui.component(`#footerAttach/footer`, { marketStatus }, footerComponent)
  }

  const updateMarketStatus = data => {
    marketStatus = data
    updateFooter()
  }

  const cbmap = {
    'data.updated.symbol': s => updateSymbols([s]),
    'data.delete.symbol': detachSymbol,
    'account.snapshot': account => {
      accounts[account.source] = account
      updateHeader()
    },
    'markets.status': updateMarketStatus,
    'account': acc => {
      account = acc

      const channel = `data.updated.positions.${acc.id}`
      cbmap[channel] = data => {
        positions = data
        updateSymbols()
      }
    }
  }

  /**
   * Setup the websocket connection
   */
  const conn = connection('ws://localhost:8888/connect')
  conn.login({ email: 'root', password: 'root' }).then( acct => {
    console.log(acct)

    conn.subscribe('message', msg => {
      if ( msg.symbol ) {
        updateSymbols([msg])
      }
    })
  })

  updateHeader()
  updateFooter()
}

/**
 * Start dashboard on window load.
 */
const init = () =>
  window.onload = () => dashboard()

export default init
