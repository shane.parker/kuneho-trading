import once from '@util/once'

let context = { parent: document }

const triggers = new Set()
const components = {}

const withContext = (ctx, cb) => {
  const octx = context

  try {
    context = ctx
    cb()
  } catch (err) {
    console.log(err)
  } finally {
    context = octx
  }
}

/**
 * Creates a template instance with bindings automatically set.
 */
const ui = {
  component: (path, props, cb, templateSelector) => {
    const existing = components[path]
    if ( existing ) {
      return existing.draw(props)
    }

    const parts = path.split('/')
    if ( parent === null ) {
      parent = document
    }

    let nparent = context.parent
    for ( let x = 0; x < parts.length - 1; x++ ) {
      const n = nparent.querySelector(parts[x])
      if ( !n ) {
        throw Error(`Cannot resolve ui element: ${path}, ${n}: not found.`)
      }
      nparent = n
    }

    const tmplname = templateSelector || cb.template
    const tmpl = document.querySelector(tmplname)
    if ( !tmpl ) {
      throw Error(`Component template not found: ${tmplname}`)
    }

    const node = document.importNode(tmpl.content, true)
    const bindings = node.querySelectorAll('[data-bind]')
    const binded = {}

    bindings.forEach(elem => {
      const bindName = elem.getAttribute('data-bind')
      const className = elem.className

      Object.defineProperty(elem, 'content', {
        set: cnt => {
          if ( elem.innerText != cnt ) {
            elem.innerText = (cnt != undefined) ? cnt : ''
          }
        }
      })
      Object.defineProperty(elem, 'classes', {
        set: cls => elem.className = `${className} ${cls}`
      })
      Object.defineProperty(elem, 'config', {
        set: config => config ?
            Object.entries(config)
              .forEach(([k, v]) => elem[k] = v) :
                (elem.classes = 'hidden', elem.content = '')
      })

      binded[bindName] = elem
    }, {})

    let stateIdx = 0

    const ctx = {
      parent: nparent,
      onced: once(),
      states: [],
      node,
      cb,

      draw: props => {
        stateIdx = 0
        withContext(ctx, () =>
          cb({bindings: binded, props, redraw: () => ctx.draw(props)}))
      },

      state: value => {
        if ( value === undefined ) {
          throw Error('Cannot set undefined state')
        }

        const idx = stateIdx++
        if ( ctx !== context ) {
          triggers.add(ctx)
        }

        const current = ctx.states[idx]
        if ( current !== undefined ) {
          return current
        }

        return (ctx.states[idx] = [value, v => (ctx.states[idx][0] = v)])
      },

      once: cb => ctx.onced(cb)
    }

    nparent.appendChild(node)

    components[path] = ctx
    ctx.draw(props)

    triggers.forEach(ctx => ctx.trigger(props))
    triggers.clear()
  },

  state: val => context.state(val),
  once: val => context.once(val),
  detached: cb => context.detached = cb,

  detach: path => {
    const ctx = components[path]
    if ( ctx === undefined ) {
      return
    }

    ctx.parent.removeChild(ctx.node)
    if ( ctx.detached ) {
      try {
        ctx.detached()
      } catch ( err ) {
        console.log(err)
      }
    }
  }
}

export default ui
