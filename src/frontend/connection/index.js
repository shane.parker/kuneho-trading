import { json, immutable } from '@util'
import timer from '@util/timer'

/**
 * Manages a persistent, authenticated websocket
 * connection and handles retries if connection closes.
 */
const connection = (url, retryTimeout = 1500) => {
  const queue = []

  const listeners = {}
  const post = (name, ...params) => {
    const set = listeners[name]
    if ( set ) {
      set.forEach(l => l(...params))
    }
  }

  let connected = false
  let reconnect = false
  let socket = null

  /**
   * Performs the authentication negotiation
   * and resolves on success.
   */
  const login = (socket, creds) => new Promise( (resolve, reject) => {
    console.log('Connected. Sending login credentials...')
    socket.send(json({ login: creds }))

    const msg = event => {
      try {
        const { account, status, reason } = JSON.parse(event.data)
        if ( (status === 'ok') && account ) {
          console.log('Authenticated.')

          socket.addEventListener('message', event =>
            post('message', JSON.parse(event.data)))

          return resolve(account)
        } else {
          console.log('Authentication error:', reason)
          socket.close()
          reject(reason)
        }
      } finally {
        socket.removeEventListener('open', open)
        socket.removeEventListener('message', msg)      
      }
    }

    socket.addEventListener('open', open)
    socket.addEventListener('message', msg)
  })

  const connect = creds => new Promise( (resolve, reject) => {
    if ( socket ) {
      return
    }

    console.log('Connecting to', url)

    socket = new WebSocket(url)
    socket.addEventListener('error', error => {
      console.log('socket error:', error)
    })
    socket.addEventListener('close', () => {
      console.log('websocket disconnected.')

      if ( connected ) {
        post('disconnected', self)
      }

      connected = false
      socket = null

      if ( reject ) {
        reject('connection error')
      }

      if ( reconnect ) {
        console.log('Retrying connection in', retryTimeout,'ms...')
        timer(retryTimeout).wait().then(() => connect(creds))
      }
    })

    socket.addEventListener('open', () => 
      login(socket, creds).then( acct => {
        reject = null
  
        connected = true
        reconnect = true
  
        resolve(acct)
    
        // Send queued up messages since last connection
        queue.forEach(msg => socket.send(json(msg)))
        queue.length = 0
      }).catch(err => reject(err)))
  })

  const self = {
    close: () => new Promise( resolve => {
      if ( socket ) {
        reconnect = false
        socket.addEventListener('close', () => {
          resolve(true)
          socket = null
        })
        socket.close()

        post('closed', self)
      } else {
        resolve(true)
      }
    }),

    subscribe: (name, cb) => {
      const set = listeners[name] || (listeners[name] = new Set())
      set.add(cb)

      return immutable({
        unsubscribe: () => {
          const set = listeners[name]
          if ( set ) {
            set.delete(cb)
          }
        }
      })
    },

    login: async creds =>
      self.close().then(() => connect(creds)),

    send: msg => {
      if ( connected ) {
        socket.send(json(msg))
      } else {
        queue.push(msg)
      }
    }
  }

  return self
}

export default connection
