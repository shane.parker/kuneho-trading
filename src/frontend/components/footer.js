import ui from '../ui'

const footer = ({
  bindings: {
    root, statusLabel
  },
  props: {
    marketStatus
  }
}) => {
  const { status } = marketStatus

  statusLabel.config = status ? {
    content: status === 'extended-hours' ? 'Extended Hours' : status,
    classes: 'connected'
  } : {
    content: 'disconnected',
    classes: 'disconnected'
  }
}
footer.template = '#footer'

export default footer
