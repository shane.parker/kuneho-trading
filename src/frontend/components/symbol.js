import { round, formatPrice } from '@util/price'
import { valueOf } from '@util'

import ui from '../ui'

const component = ({
  bindings: {
    root, header, changeAmt, changePct, position,
    changes, price, open, high, low, name,
    ticker: tickerElem
  },
  props: {
    marketStatus,
    data: {
      details = {}, ticker, positions, symbol
    }
  }, redraw
},) => {
  const [timer, setTimer] = ui.state(null)
  
  clearTimeout(timer)
  setTimer(setTimeout(() => redraw(), 1000))
  ui.detached(() => clearTimeout(timer))

  root.classes = symbol
  header.content = symbol || '???'
  name.content = Object.values(details)
    .reduce((acc, v) => v.name || acc, null)

  const postotal = Object.values(positions || {})
    .reduce( (acc, v) => (acc + v.quantity), 0)

  position.config = (postotal > 0) ? {
    classes: 'visible',
    content: postotal
  } : null

  tickerElem.classes = (ticker !== undefined) ? 'visible' : 'hidden'
  if ( ticker ) {
    const { timestamp, current, prev } = ticker
    
    const cprice = formatPrice(current.price)
    const priceTxt = `${cprice} USD`

    if ( priceTxt != price.innerText ) {
      price.content = priceTxt

      root.classList.add('updated')
      setTimeout(() => {
        root.classList.remove('updated')
      }, 100)
    }

    const changeAmtVal = cprice - round(prev.close)
    const changePctVal = (changeAmtVal / round(prev.close)) * 100.00

    changeAmt.content = `${formatPrice(changeAmtVal)}`
    changePct.content = `${formatPrice(changePctVal)}%`

    const marginAmt = 0.50

    if ( changePctVal > marginAmt ) {
      root.classList.add('positive')
    } else if ( changePctVal < -marginAmt ) {
      root.classList.add('negative')
    }

    if ( (Date.now() - timestamp) > 30000 ) {
      root.classList.add('outdated')
    }

    open.content = formatPrice(current.open)
    high.content = formatPrice(current.high)
    low.content = formatPrice(current.low)
  } else {
    root.classList.add('noTicker')
  }
}
component.template = '#symbol'

export default component
