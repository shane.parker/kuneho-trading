import { price } from '@util/price'

import ui from '../ui'

const headerAccount = ({
  bindings: {
    source, equity, cash, market_value, root
  },
  props
}) => {
  const { currency } = props

  ui.once(() => setTimeout(() => root.classes = `opaque ${props.source}`), 10)
  source.content = props.source
  
  equity.content = price(props.equity, currency)
  cash.content = price(props.cash, currency)
  market_value.content = price(props.market_value, currency)
}
headerAccount.template = '#headerAccount'

const header = ({
  bindings: {},
  props: {
    accounts
  }
}) => {
  /**
   * Render account list for headers
   */
  Object.entries(accounts).forEach( ([id, account]) => {
    ui.component(`.header .accounts/${id}`, account, headerAccount)
  })  
}
header.template = '#header'

export default header
