const { immutable } = require('./index')

/**
 * Simple key/value object memory cache
 * 
 * @param {*} ttl Time to keep objects in cache before they are purged on next cleanup
 */
const cache = (opts = {}) => {
  const {ttl = 30000, debug = false} = opts
  const store = new Map()

  const self = immutable({
    set: (key, value) => {
      store.set(key, {expire: Date.now()+ttl, value})
      return self
    },

    setEntries: entries => {
      for ( const [k, v] of entries ) {
        self.set(k, v)
      }
      return self
    },

    deleteKeys: keys => {
      for ( const k of keys ) {
        self.delete(k)
      }
      return self
    },

    get: (key, defaultValue) => {
      const entry = store.get(key)
      if ( entry === undefined ) {
        if ( debug ) {
          console.log('cache miss:', key)
        }
        return defaultValue
      }

      if ( entry.expire <= Date.now() ) {
        if ( debug ) {
          console.log('cache purge:', key)
        }
        store.delete(key)
        return defaultValue
      }

      return entry.value
    },

    delete: key => {
      store.delete(key)
      return key
    },

    cleanup: () => {
      const now = Date.now()

      for ( const [key, entry] of store ) {
        if ( entry.expire <= now ) {
          if ( debug ) {
            console.log('cache purge:', key)
          }
          store.delete(key)
        }
      }

      return self
    },
  })

  setInterval(self.cleanup, ttl)

  return self
}

module.exports = cache
