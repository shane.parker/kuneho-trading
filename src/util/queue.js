const { immutable } = require('./index')

const queue = (name = 'unnamed', start = false) => {
  let started = false
  let signal = null
  const cmds = []

  const task = async () => {
    for ( ;; ) {
      while ( cmds.length > 0 ) {
        const [cmd, resolve, reject] = cmds.shift()

        try {
          resolve(await cmd())
        } catch ( err ) {
          console.log('Uncaught exception in queue', name, ':', err)
          reject(err)
        }
      }

      await new Promise( resolve => {
        signal = resolve
      })
      signal = null
    }
  }

  const self = immutable({
    push: cmd => new Promise((resolve, reject) => {
      cmds.push([cmd, resolve, reject])
      if ( signal ) {
        signal()
      }
    }),
    start: () => {
      if ( !started ) {
        started = true
        task()
      }
    }
  })

  if ( start ) {
    self.start()
  }

  return self
}

module.exports = queue
