const fetch = (typeof window === 'undefined') ? require('node-fetch') : window.fetch

const immutable = obj => Object.freeze(obj)
const json = obj => JSON.stringify(obj)

const except = e => {
  throw e
}

const clone = v => {
  if ( Array.isArray(v) ) {
    return v.map(e => clone(e))
  } else if ( v && (typeof v === 'object') ) {
    return Object.entries(v).reduce( (acc, [k, v]) =>
      (acc[k] = clone(v), acc), {})
  }

  return v
}

const uuid = () => 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
  const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
  return v.toString(16);
});

const walk = (object, path, create = false) => {
  const parts = path.split('.')

  let x
  for ( x = 0; x < parts.length-1; x++ ) {
    const key = parts[x]

    let next = object[key]
    if ( next === undefined ) {
      if ( create ) {
        next = (object[key] = {})
      } else {
        return undefined
      }
    }

    object = next
  }

  return {object, key: parts[x]}
}

const pathValue = (obj, path, defaultValue) => {
  const res = walk(obj, path)
  if ( res === undefined ) {
    return defaultValue
  }

  const { object, key } = res
  const val = object[key]
  
  return (val === undefined) ? defaultValue : val
}

const mutate = (obj, kv) =>
  Object.entries(kv).reduce( (acc, [k, v]) => {
    const res = walk(acc, k, v !== undefined)
    if ( !res && (v !== undefined) ) {
      throw Error(`invalid key: ${k}`)
    }
    
    const { key, object } = res || {}
    if ( v === undefined ) {
      console.log(obj, kv)
      delete object[key]
    } else {
      if ( v && typeof v === 'object' ) {
        const current = object[key] || {}
        object[key] = {...current, ...v}
      } else {
        object[key] = v
      }
    }

    return acc
  }, clone(obj))

const valueOf = v =>
  (typeof v === 'function') ? v() : v

const tryValueOf = (v, defaultValue) => {
  try {
    return valueOf(v)
  } catch ( err ) {
    return defaultValue
  }
}

const collect = (arr, log = console.log) =>
  Promise.allSettled(arr)
    .then(r => r.filter( ({ value, reason }) => (value !== undefined) || log(reason) ))
    .then(m => m.map( ({ value }) => value ))

const fetchURLs = (arr, log = console.log) =>
  collect(arr.map(r => fetch(r)), log)

const fetchJSON = (arr, log) =>
  fetchURLs(arr, log)
    .then(r => collect(r.map(r => r.json())))

const sleep = timeout =>
  new Promise(resolve => setTimeout(resolve, timeout))

module.exports = {
  fetchURLs,
  fetchJSON,
  immutable,
  pathValue,
  tryValueOf,
  collect,
  valueOf,
  mutate,
  except,
  sleep,
  clone,
  json,
  uuid,
  walk
}
