const { immutable, sleep } = require('./index')

const semaphore = require('./semaphore')
const once = require('./once')

const task = (cb, {
  name = 'unnamed',
  started = false,
  interval = 1000,
  log = console.log,
  debug = false
} = {}) => {
  const sem = semaphore()
  const onced = once()
  let shutdown

  const runner = async self => {
    for ( ;; ) {
      try {
        if ( started ) {
          if ( debug ) {
            log('Running task', name)
          }

          await cb(self)
        }
      } catch ( err ) {
        self.log(`Unhandled exception:`, err)
      } finally {
        /**
         * Wait for either a trigger or for the interval
         * to pass. Whichever is first.
         */
        if ( debug ) {
          self.log(`Waiting for ${interval} milliseconds.`)
        }

        await Promise.race([ sem.wait(), sleep(interval) ])
        if ( started === false ) {
          self.log('Task terminated.')
          if ( shutdown ) {
            try {
              shutdown()
            } catch ( err ) {
              self.log('Unhandled exception during shutdown:', err)
            }
          }
          return
        }
      }
    }
  }

  const self = immutable({
    start: () => {
      if ( started === false ) {
        if ( debug ) {
          log('Starting task', name, 'with interval', interval)
        }

        started = true
        runner(internal)
      }
    },

    awake: () => {
      sem.post()
    },

    setInterval: time => interval = time,

    stop: () => {
      started = false
      self.awake()
    }
  })

  const internal = immutable(Object.create(self, {
    log: {
      value: (...params) => log(`[Task ${name}]:`, ...params)
    },

    once: {
      value: onced
    },

    shutdown: {
      value: cb => shutdown = cb
    }
  }))

  if ( started === true ) {
    if ( debug ) {
      log('Automatically starting task', name)
    }

    runner(internal)
  }

  return self
}

module.exports = task
