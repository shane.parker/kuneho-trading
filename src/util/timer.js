const { immutable } = require('@util')
const semaphore = require('@util/semaphore')

const timer = timeout => {
  const sem = semaphore()

  const to = setTimeout(() => sem.post(), timeout)

  return immutable({
    wait: () => sem.wait(),
    cancel: () => clearTimeout(to)
  })
}

module.exports = timer
