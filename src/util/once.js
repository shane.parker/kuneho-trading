const NotOnced = Symbol('Not called')

const once = () => {
  let value = NotOnced

  return cb => (value === NotOnced) ?
    (value = cb()) : value
}

module.exports = once
