const { immutable } = require('@util')

const semaphore = (count = 0) => {
  let trigger
  let promise

  const reset = () =>
    new Promise( resolve => {
      trigger = () => resolve()
    })
  promise = reset()

  const self = immutable({
    post: () => {
      count += 1
      if ( count > 0 ) {
        trigger()
      }
    },

    wait: async () => {
      if ( count === 0 ) {
        await promise
        promise = reset()
      } else {
        count -= 1
      }
    }
  })

  return self
}

module.exports = semaphore
