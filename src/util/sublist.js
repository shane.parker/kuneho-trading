const { immutable } = require('@util')

const sublist = (pubsub, subs = []) => immutable({
  subscribe: (name, cb) =>
    subs.push(pubsub.subscribe(name, cb)),

  unsubscribe: () =>
    subs.forEach(s => s.unsubscribe())
})

module.exports = sublist
