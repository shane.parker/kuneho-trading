const { immutable } = require('@util')

const monitor = (count = 0, cb) => {
  let trigger
  let promise

  const reset = () =>
    new Promise( resolve => {
      trigger = () => resolve(true)
    })
  promise = reset()

  const self = immutable({
    increment: () => {
      count += 1
      return self
    },

    decrement: () => {
      count -= 1
      if ( count === 0 ) {
        trigger()
      }
      return self
    },

    wait: async () => {
      if ( count >= 0 ) {
        await promise
        promise = reset()

        return cb ? cb() : self
      }
    },

    get count() {
      return count
    }
  })

  if ( cb ) {
    self.wait()
  }

  return self
}

module.exports = monitor
