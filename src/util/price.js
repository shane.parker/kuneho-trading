export const round = n => Math.round((n + Number.EPSILON)*100)/100
export const formatPrice = n => round(n || 0).toFixed(2)

export const price = (amt, currency = 'USD') => 
  new Intl.NumberFormat('en-US', {style: 'currency', currency: currency}).format(amt)
