const path = require('path')

module.exports = {
  mode: 'development',

  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js'
  },

  resolve: {
    alias: {
      '@util': path.resolve(__dirname, 'src/util/')
    }
  }
}
